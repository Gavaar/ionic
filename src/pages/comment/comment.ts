import { Component } from '@angular/core';
import { Comment } from '../../shared/comment';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';

/**
 * Generated class for the CommentPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-comment',
  templateUrl: 'comment.html',
})
export class CommentPage {

  comments: FormGroup;
  newComment: Comment;

  constructor(public navCtrl: NavController, public navParams: NavParams,
              public viewCtrl: ViewController,
              private formBuilder: FormBuilder) {
        
                this.comments = this.formBuilder.group({
                  author: ['', Validators.required],
                  rating: [5, Validators.required],
                  comment: ['', Validators.required]
                });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CommentPage');
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }

  onSubmit() {

    this.newComment = this.comments.value;
    this.newComment.date = new Date().toISOString();
    this.viewCtrl.dismiss(this.newComment);

  }

}
