import { Component, Inject } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, ActionSheetController, ModalController, Modal } from 'ionic-angular';
import { Dish } from '../../shared/dish';
import { Comment } from '../../shared/comment';
import { FavoriteProvider } from '../../providers/favorite/favorite';
import { CommentPage } from '../../pages/comment/comment';

/**
 * Generated class for the DishdetailPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-dishdetail',
  templateUrl: 'dishdetail.html',
})
export class DishdetailPage {

  dish: Dish;
  favorite: boolean;
  errMess: string;
  avgstars: string;
  numcomments: number;

  constructor(public navCtrl: NavController, public navParams: NavParams,
              public modalCtrl: ModalController,
              private favoriteservice: FavoriteProvider,
              private toastCtrl: ToastController,
              private actionSheet: ActionSheetController,
              @Inject('BaseURL') private BaseURL) {
                this.dish = navParams.get('dish');
                this.favorite = favoriteservice.isFavorite(this.dish.id);
                this.numcomments = this.dish.comments.length;
                let total = 0;
                this.dish.comments.forEach(comment => total += comment.rating);
                this.avgstars = (total/this.numcomments).toFixed(2);
  }

  addToFavorites () {
    console.log('Adding to Favorites', this.dish.id);
    this.favorite = this.favoriteservice.addFavorite(this.dish.id);
    this.toastCtrl.create({
      message: 'Dish ' + this.dish.id + ' added as favorite successfully',
      position: 'middle',
      duration: 3000}).present();
  }

  presentActionSheet() {
    let actionSheet = this.actionSheet.create({
      title:'More',
      buttons: [
        {
          text: 'Add to Favorites',
          handler: () => this.addToFavorites()
        },
        {
          text: 'Add a Comment',
          handler: () => this.addComment()
        },
        {
          text: 'Cancel',
          role: 'cancel',
          handler: () => console.log('-More- operation cancelled')
        }]
    });

    actionSheet.present();
  }

  addComment() {
    let modal = this.modalCtrl.create(CommentPage);
    modal.present();
    modal.onDidDismiss(newComment => {if (newComment) this.dish.comments.push(newComment); });
    console.log(this.dish);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DishdetailPage');
  }

}
